class Album < ActiveRecord::Base
  validates :name, :band_id, :year, presence: true 
  validates :name, uniqueness: { scope: :band_id }
  
  belongs_to :band
  has_many :tracks, dependent: :destroy
end
