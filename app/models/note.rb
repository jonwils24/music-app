class Note < ActiveRecord::Base
  belongs_to :track
  belongs_to :user
  
  validates :track_id, :user_id, :content, presence: true
end
