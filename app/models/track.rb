class Track < ActiveRecord::Base
  validates :name, :album_id, :track_num, :lyrics, presence: true
  validates :track_num, uniqueness: { scope: :album_id }
  
  belongs_to :album
  has_one :band, through: :album, source: :band
  has_many :notes, dependent: :destroy
end
