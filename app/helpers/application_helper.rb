module ApplicationHelper
  def ugly_lyrics(lyrics)
    better_format = ""
    lyrics.lines.each do |line|
      better_format << "&#9835; #{h(line)}"
    end
    
    "<pre>#{better_format}</pre>".html_safe
  end
end
