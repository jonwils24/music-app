MusicApp::Application.routes.draw do
  root to: redirect("/bands")
  

  resource :session, only: [:new, :create, :destroy]
  resources :users, only: [:create, :show, :new]
  resources :tracks, except: [:index, :new]
  resources :notes, only: [:create, :destroy]
  
  resources :bands do
    resources :albums, only: [:new]
  end
  
  resources :albums, except: [:index] do
    resources :tracks, only: [:new]
  end
end
