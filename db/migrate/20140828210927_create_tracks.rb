class CreateTracks < ActiveRecord::Migration
  def change
    create_table :tracks do |t|
      t.string :name, null: false
      t.references :album, index: true, null: false
      t.integer :track_num, null: false
      t.text :lyrics, null: false
      

      t.timestamps
    end
  end
end
