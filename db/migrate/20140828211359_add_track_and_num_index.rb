class AddTrackAndNumIndex < ActiveRecord::Migration
  def change
    add_index :tracks, [:album_id, :track_num], unique: true
  end
end
